/**
 * Links the path `/deadsimplechat` to a route named `deadsimplechat`. Named like this, a
 * route with the same name needs to be created in the `routes` directory.
 */
export default function () {
  this.route('deadsimplechat', { path: '/deadsimplechat' });
}