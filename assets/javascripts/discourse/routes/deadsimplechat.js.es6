/**
 * Route for the path `/deadsimplechat` as defined in `../deadsimplechat-route-map.js.es6`.
 */
export default Ember.Route.extend({
  renderTemplate() {
    // Renders the template `../templates/deadsimplechat.hbs`
    this.render('deadsimplechat');
  }
});