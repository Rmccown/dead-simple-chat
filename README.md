# deadsimplechat

DeadSimpleChat is an plugin for to implement the DeadSimpleChat embedded live chat into a Discourse site.

## Installation

### Install the plugin

Follow [Install a Plugin](https://meta.discourse.org/t/install-a-plugin/19157)
how-to from the official Discourse Meta, using `git clone https://github.com/rmccown/deadsimplechat.git`
as the plugin command.

### Create an account and chatroom
Go to [DeadSimpleChat](https://deadsimplechat.com) and create an account, then create a chatroom.  Copy the chatroom URL and paste it into the plugin config, then enable the plugin.

### Custom CSS
You may want to use the following custom CSS on the DeadSimpleChat chatroom.

```
.is-info {
  visibility: hidden;
}
.bannedUserBtn {
  display: none;
}
.chat-message {
  margin-top: 0px;
}
.chat-message .button {
  display: none;
}
.message-username a {
  display: none;
}
.online-user + a {
  display: none;
}

```

### Hide various UI elements

On the chatroom settings page, I have the following elements disabled:

* Invite User Button
* Profile Pics
* Claim Profile/Edit Profile Button
* Login Fields
* Hide Header

## Feedback

If you have issues or suggestions for the plugin, please bring them up on
[Discourse Meta](https://meta.discourse.org).

## TODO

* Some stuff, I'm sure...
