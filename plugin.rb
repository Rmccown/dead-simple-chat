# name: deadsimplechat
# version: 0.1.0
# about: A simple plugin to embed DeadSimpleChat in your site
# authors: Bob McCown

enabled_site_setting :deadsimplechat_enabled

after_initialize do
  load File.expand_path('../app/controllers/deadsimplechat_controller.rb', __FILE__)

  Discourse::Application.routes.append do
    get '/deadsimplechat' => 'deadsimplechat#index'
  end
end